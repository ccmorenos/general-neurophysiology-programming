"""Script to fet th database."""
from ca1_models.minimal_plus_model.block_feature import block_feature
import matplotlib.pyplot as plt
import argparse


def plot_hist(data, bins, limits, output_file, plot_bool=True):
    """
    Plot the histogram of the variable data.

    Arguments:
    ---------
    data: array like
        The data to be plotted
    bins: int
        The number of bins in the histograms.
    limits: array like
        The limits of the x axis in the histogram.
    output_file: string
        The file to save the histogram.
    plot_bool: bool
        Bool to proceed with the plotting

    """
    if not plot_bool:
        return

    plt.hist(data, bins=bins, range=limits)
    plt.savefig(output_file)
    plt.clf()


# Set parser for the console arguments.
parser = argparse.ArgumentParser(description='Create the PYR database.',
                                 formatter_class=argparse.RawTextHelpFormatter)

# Set arguments.
parser.add_argument("--rheo", dest="rheo", action="store_true", default=False,
                    help="Create the Rheo database for the PYR neurons.")

parser.add_argument("--sfa", dest="sfa", action="store_true", default=False,
                    help="Create the SFA database for the PYR neurons.")

parser.add_argument("--pir", dest="pir", action="store_true", default=False,
                    help="Create the PIR database for the PYR neurons.")

parser.add_argument("--all", dest="all", action="store_true", default=False,
                    help="Create all the database for the PYR neurons.\n" +
                    "Not recommended.")

parser.add_argument("--plot", dest="plot", action="store_true", default=False,
                    help="Plot the histograms for the database.")

# Parse arguments.
args = parser.parse_args()

# Create database.
# All the database.
if args.all:
    all_database = block_feature.create_database()
    all_database.to_csv("database.csv")
    plot_hist(all_database.Rheo, 15, [0, 7], "all_Rheo.svg", args.plot)
    plot_hist(all_database.PIR, 50, [-25, 0], "all_PIR.svg", args.plot)
    plot_hist(all_database.SFA*1000, 14, [-0.1, 0.6], "all_SFA.svg", args.plot)

if args.rheo:
    rheo_database = block_feature.create_Rheo_database()
    rheo_database.to_csv("Rheo.csv")
    plot_hist(rheo_database.Rheo, 15, [0, 7], "Rheo.svg", args.plot)

if args.pir:
    pir_database = block_feature.create_PIR_database()
    pir_database.to_csv("PIR.csv")
    plot_hist(pir_database.PIR, 50, [-25, 0], "PIR.svg", args.plot)

if args.sfa:
    sfa_database = block_feature.create_SFA_database()
    sfa_database.to_csv("SFA.csv")
    plot_hist(sfa_database.SFA*1000, 14, [-0.1, 0.6], "SFA.svg", args.plot)
