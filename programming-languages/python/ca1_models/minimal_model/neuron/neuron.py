"""The neuron class."""


class neuron:
    """
    The basic class for a neuron.

    This class, based on the model exposed in the papers:

    * Experimentally constrained CA1 fast-firing parvalbumin-positive
      interneuron network models exhibit sharp transitions into coherent
      high frequency rhythms.
      https://doi.org/10.3389/fncom.2013.00144

    * Combining Theory, Model, and Experiment to Explain How Intrinsic Theta
      Rhythms Are Generated in an In Vitro Whole Hippocampus Preparation
      without Oscillatory Inputs.
      https://dx.doi.org/10.1523%2FENEURO.0131-17.2017

    simulates a neuron of the CA1 region in the hippocampus.

    """

    def __init__(self, V_i, u_i, C_m, k_low, k_high,
                 v_r, v_t, v_peak, a, b, c, d):
        """
        Neuron constructor, with all the parameters needed.

        This constructor creates a neuron of the CA1 region in the hippocampus.
        The parameters correspond to all the parameters of the differential
        equation that model the neurons as well as the initial conditions.

        Arguments:
        ---------
        V_i: float
            Initial condition for the membrane potential. [mV]
        u_i: float
            Initial condition for the slow "recovery" current. [pA]
        C_m: float
            The membrane capacitance. [pF]
        k_low: float
            Low scaling factor. [nS/mV]
        k_high: float
            High scaling factor. [nS/mV]
        v_r: float
            The resting membrane potential. [mV]
        v_t: float
            The Instantaneous threshold potential. [mV]
        v_peak: float
            The spike cut-off potential. [mV]
        a: float
            The recovery time constant of the adaptation current. [ms^-1]
        b: float
            The sensitivity of the adaptation current to subthresholds
            fluctuations. [nS]
        c: float
            Voltage reset value. [mV]
        d: float
            The total amount of backwards and inwards currents activated
            during the spike, that affects the after-spike behavior. [pA]

        """
        # Set parametes.
        self.C_m = C_m

        self.k_low = k_low
        self.k_high = k_high

        self.v_r = v_r
        self.v_t = v_t
        self.v_peak = v_peak

        self.a = a
        self.b = b
        self.c = c
        self.d = d

        # Set initial conditions.
        self.V = V_i
        self.u = u_i

        # Calculate k scaling factor.
        self.k = k_low
        self.calculate_k()

    def calculate_k(self):
        """Calculate the k scaling factor respect membrane potential V."""
        if self.V <= self.v_t:
            self.k = self.k_low
        else:
            self.k = self.k_high

    def check_peak(self):
        """
        Check peak status.

        Check the status of the membrane potential respect to peak cut-off. If
        the membrane potential is above the peak cut-off, it is reseted.

        """
        if self.V >= self.v_peak:
            # Update V and u.
            self.V = self.c
            self.u += self.d

            # Return confirmation of the peak.
            return True
        else:
            return False

    def V_diff(self, t, k_V=0, k_u=0, I_syn=0, I_other=0):
        """
        Differential equation for V.

        Differential equation describing the membrane potential:

              [k * (V - v_r) * (V - v_t) - u + I_other - I_syn]
        V' = ---------------------------------------------------
                                     C_m

        Arguments:
        ---------
        t: float
            The time. [ms]
        k_V: float
            k fator for V, in case of Runge-Kutta implementation.
        k_u: float
            k fator for u, in case of Runge-Kutta implementation.
        I_syn: float
            Synaptic input. [pA]
        I_other: float
            Synaptic from the "other" source (noise). [pA]

        Return:
        ------
        The value for V'.

        """
        # Get the parameters.
        C_m = self.C_m
        k = self.k
        v_r = self.v_r
        v_t = self.v_t

        # Get actual states of the V and u variables.
        V = self.V + k_V
        u = self.u + k_u

        return 1/C_m * (k * (V - v_r) * (V - v_t) - u + I_other - I_syn)

    def u_diff(self, t, k_V=0, k_u=0):
        """
        Differential equation for u.

        Differential equation for the slow "recovery" current:

        u' = a [b * (V - v_r) - u]

        Arguments:
        ---------
        t: float
            The time. [ms]
        k_V: float
            k fator for V, in case of Runge-Kutta implementation.
        k_u: float
            k fator for u, in case of Runge-Kutta implementation.

        Return:
        ------
        The value for u'.

        """
        # Get the parameters.
        v_r = self.v_r
        a = self.a
        b = self.b

        # Get actual states of the V and u variables.
        V = self.V + k_V
        u = self.u + k_u

        return a * (b * (V - v_r) - u)


class PYR_neuron(neuron):
    """
    The class for a PYR neuron.

    This class simulate a pyramidal (PYR) neuron of the CA1 region in the
    hippocampus.

    """

    def __init__(self, V_i, u_i, C_m=115.0, k_low=0.1, k_high=3.3,
                 v_r=(-61.8), v_t=(-57.0), v_peak=22.6, a=0.0012, b=3.0,
                 c=(-65.8), d=10.0):
        """
        Pyramidal neuron constructor.

        This constructor creates a pryamidal neuron of the CA1 region in the
        hippocampus. The parameters correspond to all the parameters of the
        differential equation that model the neurons as well as the initial
        conditions.

        Arguments:
        ---------
        V_i: float
            Initial condition for the membrane potential. [mV]
        u_i: float
            Initial condition for the slow "recovery" current. [pA]
        C_m: float
            The membrane capacitance. [pF]
        k_low: float
            Low scaling factor. [nS/mV]
        k_high: float
            High scaling factor. [nS/mV]
        v_r: float
            The resting membrane potential. [mV]
        v_t: float
            The Instantaneous threshold potential. [mV]
        v_peak: float
            The spike cut-off potential. [mV]
        a: float
            The recovery time constant of the adaptation current. [ms^-1]
        b: float
            The sensitivity of the adaptation current to subthresholds
            fluctuations. [nS]
        c: float
            Voltage reset value. [mV]
        d: float
            The total amount of backwards and inwards currents activated
            during the spike, that affects the after-spike behavior. [pA]

        """
        super().__init__(V_i, u_i, C_m, k_low, k_high,
                         v_r, v_t, v_peak, a, b, c, d)


class PV_plus_neuron(neuron):
    """
    The class for a PV+ neuron.

    This class simulate a parvalbumin positive (PV+) neuron of the CA1 region
    in the hippocampus.

    """

    def __init__(self, V_i, u_i, C_m=90.0, k_low=1.7, k_high=14.0,
                 v_r=(-60.6), v_t=(-43.1), v_peak=(-2.5), a=0.1, b=(-0.1),
                 c=(-67.0), d=0.1):
        """
        Parvalbumin-expressing neuron constructor.

        This constructor creates a parvalbumin positive neuron of the CA1
        region in the hippocampus. The parameters correspond to all the
        parameters of the differential equation that model the neurons as well
        as the initial conditions.

        Arguments:
        ---------
        V_i: float
            Initial condition for the membrane potential. [mV]
        u_i: float
            Initial condition for the slow "recovery" current. [pA]
        C_m: float
            The membrane capacitance. [pF]
        k_low: float
            Low scaling factor. [nS/mV]
        k_high: float
            High scaling factor. [nS/mV]
        v_r: float
            The resting membrane potential. [mV]
        v_t: float
            The Instantaneous threshold potential. [mV]
        v_peak: float
            The spike cut-off potential. [mV]
        a: float
            The recovery time constant of the adaptation current. [ms^-1]
        b: float
            The sensitivity of the adaptation current to subthresholds
            fluctuations. [nS]
        c: float
            Voltage reset value. [mV]
        d: float
            The total amount of backwards and inwards currents activated
            during the spike, that affects the after-spike behavior. [pA]

        """
        super().__init__(V_i, u_i, C_m, k_low, k_high,
                         v_r, v_t, v_peak, a, b, c, d)
