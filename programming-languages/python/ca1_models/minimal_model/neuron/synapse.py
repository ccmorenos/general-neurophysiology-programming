"""The synapse class."""


class synapse:
    """
    This class simulate a sinapsis.

    This is a base class for all the system synapses, based on the model
    exposen in the papers:

    * Experimentally constrained CA1 fast-firing parvalbumin-positive
      interneuron network models exhibit sharp transitions into coherent
      high frequency rhythms.
      https://doi.org/10.3389/fncom.2013.00144

    * Combining Theory, Model, and Experiment to Explain How Intrinsic Theta
      Rhythms Are Generated in an In Vitro Whole Hippocampus Preparation
      without Oscillatory Inputs.
      https://dx.doi.org/10.1523%2FENEURO.0131-17.2017

    """

    def __init__(self, tau_R, tau_D, g, E_rev):
        """
        Synapse constructor.

        This class create a synapse between two neurons.

        Arguments:
        ---------
        tau_R: float
            Rise time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        tau_D: float
            Decay time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        g: float
            Maximal synaptic conductance from the presynaptic neurons to the
            postsinaptic neurons. [nS]
        E_rev: float
            The reversal potential potential of the synapse. [mV]

        """
        self.tau_R = tau_R
        self.tau_D = tau_D
        self.g = g
        self.E_rev = E_rev

    def get_the_synaptic_input(self, s, V):
        """
        Get the synaptic input current of the synapse.

        Arguments:
        ---------
        s: float
            The fraction of the open synaptic channel. [dimensionless]
        V: float
            The postsinaptic neuron membrane potential. [mV]

        Return:
        ------
        The synaptic input current.

        """
        return self.g * s * (V - self.E_rev)


class PYR_to_PYR_synapse(synapse):
    """This class simulate a sinapsis from PYR to PYR neurons."""

    def __init__(self, tau_R=0.5, tau_D=3.0, g=0.094, E_rev=(-15)):
        """
        PYR -> PYR synapse constructor.

        This class create a synapse from a pyramidal (PYR) neuron to a
        pyramidal (PYR) neurons. Since this sypanse is exitatory, the reversal
        potential is the exitatory one.

        Arguments:
        ---------
        tau_R: float
            Rise time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        tau_D: float
            Decay time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        g: float
            Maximal synaptic conductance from the presynaptic neurons to the
            postsinaptic neurons. [nS]
        E_rev: float
            The reversal potential potential of the synapse. [mV]

        """
        super().__init__(tau_R, tau_D, g, E_rev)


class PYR_to_PV_plus_synapse(synapse):
    """This class simulate a sinapsis from PYR to PV+ neurons."""

    def __init__(self, tau_R=0.37, tau_D=2.1, g=3.0, E_rev=(-15)):
        """
        PYR -> PV+ synapse constructor.

        This class create a synapse from a pyramidal (PYR) neuron to a
        parvalbumin positive (PV+) neuron. Since this sypanse is exitatory,
        the reversal potential is the exitatory one.

        Arguments:
        ---------
        tau_R: float
            Rise time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        tau_D: float
            Decay time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        g: float
            Maximal synaptic conductance from the presynaptic neurons to the
            postsinaptic neurons. [nS]
        E_rev: float
            The reversal potential potential of the synapse. [mV]

        """
        super().__init__(tau_R, tau_D, g, E_rev)


class PV_plus_to_PYR_synapse(synapse):
    """This class simulate a sinapsis from PV+ to PYR neurons."""

    def __init__(self, tau_R=0.3, tau_D=3.5, g=8.7, E_rev=(-85)):
        """
        PV+ -> PYR synapse constructor.

        This class create a synapse from a parvalbumin positive (PV+) neuron
        to a pyramidal (PYR) neuron. Since this sypanse is inhibitory, the
        reversal potential is the inhibitory one.

        Arguments:
        ---------
        tau_R: float
            Rise time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        tau_D: float
            Decay time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        g: float
            Maximal synaptic conductance from the presynaptic neurons to the
            postsinaptic neurons. [nS]
        E_rev: float
            The reversal potential potential of the synapse. [mV]

        """
        super().__init__(tau_R, tau_D, g, E_rev)


class PV_plus_to_PV_plus_synapse(synapse):
    """This class simulate a sinapsis from PV+ to PV+ neurons."""

    def __init__(self, tau_R=0.27, tau_D=1.7, g=3.0, E_rev=(-85)):
        """
        PYR -> PV+ synapse constructor.

        This class create a synapse from a parvalbumin positive (PV+) neuron
        to a parvalbumin positive (PV+) neuron. Since this sypanse is
        inhibitory, the reversal potential is the inhibitory one.

        Arguments:
        ---------
        tau_R: float
            Rise time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        tau_D: float
            Decay time constant from the presynaptic neurons to the
            postsinaptic neurons. [ms]
        g: float
            Maximal synaptic conductance from the presynaptic neurons to the
            postsinaptic neurons. [nS]
        E_rev: float
            The reversal potential potential of the synapse. [mV]

        """
        super().__init__(tau_R, tau_D, g, E_rev)
