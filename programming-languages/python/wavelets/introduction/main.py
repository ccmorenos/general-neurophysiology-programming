"""A wavlet test."""
from scipy.signal import cwt, morlet
import matplotlib.pyplot as plt
from haar import HaarWavelets
import numpy as np
import pywt

N = 1024

n = np.arange(0, N)
dn = 1.0
m = (n-1)/N
signal = 20 * m**2 * (m-1)**4 * np.cos(12*np.pi*m)

hw = HaarWavelets(signal)
hw.plot_signal("signal")
hw.plot_wavelet("wavelet")
hw.plot_scaling("scaling")
hw.plot_haar_transform("haar")
hw.plot_compress("compress")
hw.plot_cumulative_E("energy")
hw.plot_transfer("transfer")

widths = np.arange(1, 12)
cwtmatr = cwt(signal, morlet, widths)
plt.imshow(
    np.abs(cwtmatr), extent=[-1, 1, 1, 15], cmap='PRGn', aspect='auto',
    vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max()
)
plt.savefig("scalogram.pdf")
plt.clf()

cwtmatr, freqs = pywt.cwt(signal, widths, "morl")
plt.pcolormesh(
    n, freqs, cwtmatr,
    shading="gouraud"
)
plt.savefig("spectrogram.pdf")
plt.clf()

N = 1024 * 64

n = np.arange(0, N)
m = (n-1)/N
signal = 20 * m**2 * (m-1)**4 * np.cos(12*np.pi*m) +\
    np.random.normal(0, 0.1, size=N)

hw = HaarWavelets(signal, 6)
hw.plot_signal("signal_noise")
hw.plot_denoise("denoise_noise")
