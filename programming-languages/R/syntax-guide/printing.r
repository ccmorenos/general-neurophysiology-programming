# Printing.

# Variables.

var.a = 12.3
var.b = 23L
var.c = TRUE
var.d = "Hi!"

# Printing.

## print(), one thing at a time.

print("Variables")
print(var.a)
print(var.b)
print(var.c)
print(var.d)

## cat(), many things in the same times.

cat("\n")
cat("Var a:", var.a, "\n")
cat("Var a:", var.a, "Var b:\n", var.b)
cat("Var a:", var.a, "\tVar b:\n", var.b)
cat(var.a, var.b, var.c, var.d, "\n")
cat("Variables:\n")
cat(ls(), "\n")
cat("\n")
