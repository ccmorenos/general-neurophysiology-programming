# Variables.

# Ways of assign variables.

## Left assignment.
a = 12
b <- 13
d <<- 14

cat("\nLeft assigment:\n")
print(a)
print(b)
print(d)

## Right assignment.
15 -> a
16 ->> b

cat("\nRight assigment:\n")
print(a)
print(b)

# Data types.

## Logic or Booleans.

a = TRUE
b = FALSE

cat("\nBoolean variables:\n")
print(a)
print(b)

## Numeric.

a = 12
b = 123.21
d = -0.123

cat("\nNumeric variables:\n")
print(a)
print(b)
print(d)

## Integer.

a = 9L
b = -15L
d = 0L

cat("\nInteger variables:\n")
print(a)
print(b)
print(d)

## Complex.

a = 1i
b = 2 + 4.5i
d = 98 - 3i

cat("\nComplex variables:\n")
print(a)
print(b)
print(d)

## Character or String.

a = "Hello"
b = 'World'
d = "..."

cat("\nCharacter variables:\n")
print(a)
print(b)
print(d)

## Raw.

a = charToRaw("Hello")
b = charToRaw("World")
d = charToRaw("!")

cat("\nRaw variables:\n")
print(a)
print(b)
print(d)

# R-Objects.

## Vectors.

myVector <- c(1.13, 5, 0.3, -2.23)
my.vector = c("Hello", "World", "!")
my_vector <<- c(TRUE, FALSE, TRUE, TRUE)
.my_vector = c(2L ,5L, -4L)

cat("\nVectors:\n")
print(myVector)
print(my.vector)
print(my_vector)
print(.my_vector)

## Lists.

myList = list(1.02, 2L, "Vector", TRUE)
my.list = list(3.45, "List", c(2.34, 23.45, 0.34, 2.12))

cat("\nLists:\n")
cat("\n - List A:\n")
print(myList)
cat("\n - List B:\n")
print(my.list)

## Matrices.

A <- matrix(c(1,2,3,4,5,6,7,8,9), ncol=3, nrow=3)
B <- matrix(c(1,2,3,4,5,6,7,8,9), ncol=3, nrow=3, byrow=TRUE)
C = matrix(
  c("H", "e", "l", "l", "o", "W", "o", "r", "l", "d"),
  nrow=2, ncol=5, byrow=TRUE
)

cat("\nMatrices:\n")
cat("\n - Matrix A:\n")
print(A)
cat("\n - Matrix B:\n")
print(B)
cat("\n - Matrix C:\n")
print(C)

## Arrays.

array.1 = array(c(1, 2, 3, 4), dim=c(8))
array.2 = array(c(34, 12, 3, 11), dim=c(3, 4))
array.3 = array(c(0, 1), dim=c(2, 2, 2))
array.4 = array(c(22, 1), dim=c(3, 2, 4, 2))
array.5 = array(c("A", "B"), dim=c(2, 2, 3, 2, 2))

cat("\nArrays:\n")
cat("\n - Dim=1:\n")
print(array.1)
cat("\n - Dim=2:\n")
print(array.2)
cat("\n - Dim=3:\n")
print(array.3)
cat("\n - Dim=4:\n")
print(array.4)
cat("\n - Dim=5:\n")
print(array.5)

## Factors.

my.factor = factor(c(2.3, 3.4, 98.0, 0.2))
myFactor <<- factor(c("a", "b", "c", "d", "e"))

cat("\nFactors:\n")
print(my.factor)
print(myFactor)

## Data Frames.

# Each argument of the data.frame function is a column.
# All columns might have a vector with the same lenght.
df = data.frame(
  sample_group = c("A", "B", "A", "B", "B", "C"),
  average_voltage = c(12.3, 28.3, 11.9, 20.9, 25.1, 11.1),
  repetitions = c(9L, 12L, 13L, 2L, 4L, 20L)
)

cat("\nData frame:\n")
print(df)

# Identify variables.

cat("\nAll variables, except hidden ones:\n")
print(ls())
cat("\nAll variables.\n")
print(ls(all.name = TRUE))
cat("\nAll variables with pattern 'my':\n")
print(ls(pattern = "my"))
cat("\nAll variables with pattern 'my', included hidden ones:\n")
print(ls(pattern = "my", all.name = TRUE))

# Removing variables.

## Remove some variables.
rm(my.factor)
rm(my.vector)
rm(a)
rm(b)
rm(A)
rm(B)

cat("\nAll variables after deleting some:\n")
print(ls(all.name = TRUE))

## Remove all variables.
rm(list = ls(all.name = TRUE))

cat("\nAll variables after deleting all:\n")
print(ls(all.name = TRUE))
