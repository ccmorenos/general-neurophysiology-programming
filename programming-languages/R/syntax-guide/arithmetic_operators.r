# Arithmetic Operators.

# Library used for color printing.
library(crayon)

cat(cyan("Arithmetic Operators\n\n"))

# Variables.

## Vectors.

vector.numerical.a = c(12.34, 12.34, 2.44, -0.2, 0.0, 3.45)
vector.numerical.b = c(11.2, 0.4, 0.0, 33.21, -0.1, -12.34)

vector.integer.a = c(1L, 2L, 3L, 4L, 5L)
vector.integer.b = c(12L, 33L, -23L, 14L, -11L)

vector.complex.a = c(1 + 1i, 1, 1i, 3 + 5i)
vector.complex.b = c(2 - 3i, 9 + 2i, 4 - 2i, -3 -4i)

## Matrices.

matrix.bool.a = matrix(
  c(
    TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE,
    TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE,
    TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE
  ),
  ncol=8, nrow=3
)
matrix.bool.b = matrix(
  c(
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE,
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE,
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE
  ),
  ncol=8, nrow=3
)
matrix.bool.c = matrix(
  c(
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE,
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE,
    TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, TRUE
  ),
  ncol=3, nrow=8
)

matrix.numerical.a = matrix(
  c(
    13.64, 67.08, 81.58, -35.88, 11.82, -12.09, 83.63, -36.91,
    142.52, -34.43, 100.01, 102.08, 14.15, -31.06, 19.44, -25.77,
    -48.64, 5.26, -43.03, 56.89, 94.59, -33.83, 33.87, -4.07
  ),
  ncol=8, nrow=3
)
matrix.numerical.b = matrix(
  c(
    5.66, 19.36, 70.33, 65.96, 69.32, 76.62, 117.40, 60.06,
    -46.50, 101.34, 68.73, 142.57, 25.74, -25.02, -0.30, -11.80,
    35.26, 116.21, 24.08, -12.99, 131.80, 39.73, 90.71, 35.71
  ),
  ncol=8, nrow=3
)
matrix.numerical.c = matrix(
  c(
    -43.10, 16.98, -35.80, 64.81, 42.31, -6.57, 104.60, 137.73,
    -10.28, -8.47, 20.36, 146.70, 56.67, 22.71, 119.09, -0.14,
    104.82, 51.15, 43.76, -36.12, -29.56, 86.04, -22.28, -22.26
  ),
  ncol=3, nrow=8
)

matrix.integer.a = matrix(
  c(
    -33, 36, -33, 74, 129, 1, 109, 118,
    41, -43, 113, -40, 116, 26, 64, -28,
    117, 122, 105, 95, 106, -11, 32, 124
  ),
  ncol=8, nrow=3
)
matrix.integer.b = matrix(
  c(
    7, 6, 58, 100, 141, 71, 5, 20, 110,
    66, -46, 37, -40, 105, 89, 66, 43,
    -16, 6, 38, 127, 92, 147, 33
  ),
  ncol=8, nrow=3
)
matrix.integer.c = matrix(
  c(
    147, -23, 82, 139, 133, 32, 51, 2,
    141, -38, 141, 9, -35, 79, 120, 0,
    79, 90, -8, 62, 28, 40, 94, 18
  ),
  ncol=3, nrow=8
)

matrix.complex.a = matrix(
  c(
    30.26 + 126.56i, 108.14 - 21.54i, -43.32 - 14.78i, 11.55 - 30.24i,
    1.36 - 28.65i, 79.16 + 69.67i, 70.85 - 12.63i, 21.35 + 11.54i,
    79.58 + 32.41i, 81.69 + 21.08i, -37.75 + 22.63i, 39.81 + 129.32i,
    7.92 + 102.12i, 129.56 - 14.23i, 70.57 - 47.64i, 107.12 - 28.91i,
    118.19 + 41.98i, -28.37 + 71.16i, 72.71 + 143.68i, 102.37 + 117.68i,
    22.04 +23.16i, -0.85 + 79.05i, -44.43 + 107.35i, 17.45 + 100.71i
  ),
  ncol=8, nrow=3
)
matrix.complex.b = matrix(
  c(
    -42.42 + 38.08i, -38.65 + 137.14i, 84.33 - 15.53i, 17.35 + 90.87i,
    16.24 + 113.30i, 144.14 + 14.11i, 68.94 + 15.54i, 18.64 - 23.92i,
    -8.88 - 21.25i, -44.75 + 77.42i, 111.77 - 35.55i, -8.48 + 8.65i,
    105.43 + 60.51i, -5.48 + 135.60i, 9.22 + 12.57i, -44.13 - 47.55i,
    -3.51 + 85.99i, -4.01 + 15.93i, 104.79 + 118.14i, 4.13 + 59.89i,
    -8.12 + 45.83i, -32.60 + 89.87i, -39.12 + 11.14i, -38.76 + 82.17i
  ),
  ncol=8, nrow=3
)
matrix.complex.c = matrix(
  c(
    -42.42 + 38.08i, -38.65 + 137.14i, 84.33 - 15.53i, 17.35 + 90.87i,
    16.24 + 113.30i, 144.14 + 14.11i, 68.94 + 15.54i, 18.64 - 23.92i,
    -8.88 - 21.25i, -44.75 + 77.42i, 111.77 - 35.55i, -8.48 + 8.65i,
    105.43 + 60.51i, -5.48 + 135.60i, 9.22 + 12.57i, -44.13 - 47.55i,
    -3.51 + 85.99i, -4.01 + 15.93i, 104.79 + 118.14i, 4.13 + 59.89i,
    -8.12 + 45.83i, -32.60 + 89.87i, -39.12 + 11.14i, -38.76 + 82.17i
  ),
  ncol=3, nrow=8
)

## Vectors.
cat(green("Vector:\n"))

cat(yellow("\nAdd:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na + b: ", vector.numerical.a + vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na + b: ", vector.integer.a + vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na + b: ", vector.complex.a + vector.complex.b, "\n\n")

cat(yellow("\nSubstract:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na - b: ", vector.numerical.a - vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na - b: ", vector.integer.a - vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na - b: ", vector.complex.a - vector.complex.b, "\n\n")

cat(yellow("\nMultiply:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na * b: ", vector.numerical.a * vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na * b: ", vector.integer.a * vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na * b: ", vector.complex.a * vector.complex.b, "\n\n")

cat(yellow("\nDivide:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na / b: ", vector.numerical.a / vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na / b: ", vector.integer.a / vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na / b: ", vector.complex.a / vector.complex.b, "\n\n")

cat(yellow("\nModulo:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na %% b: ", vector.numerical.a %% vector.numerical.b, "\n\n")
cat("b: ", vector.integer.b, "\ta: ", vector.integer.a,
"\nb %% a: ", vector.integer.b %% vector.integer.a, "\n\n")

cat(yellow("\nRounded division:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na %/% b: ", vector.numerical.a %/% vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na %/% b: ", vector.integer.a %/% vector.integer.b, "\n\n")

cat(yellow("\nPower:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na ^ b: ", vector.numerical.a ^ vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na ^ b: ", vector.integer.a ^ vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na ^ b: ", vector.complex.a ^ vector.complex.b, "\n\n")

## Matrices.
cat(green("\n==============================================================\n"))
cat(green("Matrices:\n"))

cat(yellow("\nAdd:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na + b:\n")
print(matrix.numerical.a + matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na + b:\n")
print(matrix.integer.a + matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na + b:\n")
print(matrix.complex.a + matrix.complex.b)
cat("\n\n")

cat(yellow("\nSubstract:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na - b:\n")
print(matrix.numerical.a - matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na - b:\n")
print(matrix.integer.a - matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na - b:\n")
print(matrix.complex.a - matrix.complex.b)
cat("\n\n")

cat(yellow("\nMultiply:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na * b:\n")
print(matrix.numerical.a * matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na * b:\n")
print(matrix.integer.a * matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na * b:\n")
print(matrix.complex.a * matrix.complex.b)
cat("\n\n")

cat(yellow("\nDivide:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na / b:\n")
print(matrix.numerical.a / matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na / b:\n")
print(matrix.integer.a / matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na / b:\n")
print(matrix.complex.a / matrix.complex.b)
cat("\n\n")

cat(yellow("\nModulo:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na %% b:\n")
print(matrix.numerical.a %% matrix.numerical.b)
cat("\n\n")
cat("b:\n")
print(matrix.integer.b)
cat("a:\n")
print(matrix.integer.a)
cat("\nb %% a:\n")
print(matrix.integer.b %% matrix.integer.a)
cat("\n\n")

cat(yellow("\nRounded division:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na %/% b:\n")
print(matrix.numerical.a %/% matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na %/% b:\n")
print(matrix.integer.a %/% matrix.integer.b)
cat("\n\n")

cat(yellow("\nPower:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na ^ b:\n")
print(matrix.numerical.a ^ matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na ^ b:\n")
print(matrix.integer.a ^ matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na ^ b:\n")
print(matrix.complex.a ^ matrix.complex.b)
cat("\n\n")

cat(yellow("\nMatrix Product:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("c:\n")
print(matrix.numerical.c)
cat("\na %*% c:\n")
print(matrix.numerical.a %*% matrix.numerical.c)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("c:\n")
print(matrix.integer.c)
cat("\na %*% c:\n")
print(matrix.integer.a %*% matrix.integer.c)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("c:\n")
print(matrix.complex.c)
cat("\na %*% c:\n")
print(matrix.complex.a %*% matrix.complex.c)
cat("\n\n")
